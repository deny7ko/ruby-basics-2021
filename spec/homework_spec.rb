require './homework'

describe 'Homework' do
  describe '#validate' do
    context 'with length check' do
      context 'when < 16' do
        let(:short_license_key) { 53082075408853 }

        it 'returns false' do
          result = validate(short_license_key)
          expect(result).to be false
        end
      end

      context 'when == 16' do
        let(:valid_license_key) { 5244403223651176 }

        it 'returns true' do
          result = validate(valid_license_key)
          expect(result).to be true
        end
      end

      context 'when > 16' do
        let(:long_license_key) { 53082075408853231 }

        it 'returns false' do
          result = validate(long_license_key)
          expect(result).to be false
        end
      end
    end

    context 'with first 2 digits check' do
      context 'when valid' do
        let(:valid_license_keys) do
          [
            5169294814153321,
            5244403223651176,
            5364967124577676,
            5436402220169339,
            5539470583285584
          ]
        end

        it 'returns true' do
          all_valid = valid_license_keys.all? { |key| validate(key) }
          expect(all_valid).to be true
        end
      end

      context 'when not valid' do
        let(:invalid_license_keys) do
          [
            4169294814153321,
            0244403223651176,
            1364967124577676,
            5036402220169339,
            8539470583285584
          ]
        end

        it 'returns false' do
          all_invalid = invalid_license_keys.none? { |key| validate(key) }
          expect(all_invalid).to be true
        end
      end
    end
  end

  context 'with checksum check' do
    context 'when valid' do
      let(:valid_license_keys) do
        [
          5169294814153321,
          5244403223651176,
          5364967124577676,
          5436402220169339,
          5539470583285584
        ]
      end

      it 'returns true' do
        all_valid = valid_license_keys.all? { |key| validate(key) }
        expect(all_valid).to be true
      end
    end

    context 'when not valid' do
      let(:invalid_license_keys) do
        [
          5169294814153320,
          5244403223651175,
          5364967124577675,
          5436402220169338,
          5539470583285583
        ]
      end

      it 'returns false' do
        all_invalid = invalid_license_keys.none? { |key| validate(key) }
        expect(all_invalid).to be true
      end
    end
  end
end
